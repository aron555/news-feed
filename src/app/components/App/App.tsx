import React, { FC, useEffect, useRef } from 'react';
import { Routes, Route, useLocation, Outlet } from 'react-router-dom';

import { Page } from '@components/Page/Page';
import { HomePage } from '@features/articlesList/components/HomePage/HomePage';
import { CategoryPage } from '@features/categoryArticles/components/CategoryPage/CategoryPage';
import { ArticlePage } from '@features/articleItem/components/ArticlePage/ArticlePage';
import { AdminPage } from '@features/admin/components/AdminPage/AdminPage';
import { AdminArticles } from '@features/admin/components/AdminArticles/AdminArticles';
import { AdminArticleItem } from '@features/admin/components/AdminArticleItem/AdminArticleItem';
import { PrivateRoute } from '@features/auth/components/PrivateRoute/PrivateRoute';
import { LoginContainer } from '@features/auth/login/LoginContainer';
import { ErrorPage } from '@components/ErrorPage/ErrorPage';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

export const App: FC = () => {
  const location = useLocation();
  const { pathname } = location;
  const prevPathName = useRef(pathname);
  const nodeRef = useRef(null);

  useEffect(() => {
    if (pathname !== prevPathName.current) {
      prevPathName.current = pathname;
      window.scrollTo(0, 0);
    }
  }, [pathname]);

  return (
    <TransitionGroup>
      <CSSTransition nodeRef={nodeRef} key={pathname} timeout={100} classNames="page-animation">
        <div ref={nodeRef}>
          <Routes location={location}>
            <Route path="/" element={<Layout />}>
              <Route index element={<HomePage />} />
              <Route path="article/:id" element={<ArticlePage />} />
              <Route path=":category" element={<CategoryPage />} />
              <Route path="login" element={<LoginContainer />} />
              <Route path="*" element={<ErrorPage />} />
            </Route>

            <Route
              path="/admin/"
              element={
                <PrivateRoute>
                  <AdminLayout />
                </PrivateRoute>
              }
            >
              <Route index element={<AdminArticles />} />
              <Route path="create/" element={<AdminArticleItem />} />
              <Route path="edit/:id" element={<AdminArticleItem />} />
              <Route path="*" element={<ErrorPage />} />
            </Route>
          </Routes>
        </div>
      </CSSTransition>
    </TransitionGroup>
  );
};

function Layout() {
  return (
    <Page>
      <Outlet />
    </Page>
  );
}

function AdminLayout() {
  return (
    <AdminPage>
      <Outlet />
    </AdminPage>
  );
}
