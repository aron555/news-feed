import { Navigate, useLocation } from 'react-router-dom';
import React from 'react';
import { useAuthContext } from '../../AuthContextProvider';
import { Box, CircularProgress } from '@mui/material';

export const PrivateRoute = ({ children }: { children: JSX.Element }) => {
  const location = useLocation();
  const { isAuthenticated } = useAuthContext();

  if (isAuthenticated === null) {
    // если статус авторизации пока неизвестен
    return (
      <Box sx={{ p: 4, textAlign: 'center' }}>
        <CircularProgress color="primary" />
      </Box>
    );
  }

  if (!isAuthenticated) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  } else {
    return children;
  }
};
