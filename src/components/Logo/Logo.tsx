import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';
import logo from '../../images/logo.svg';
import './Logo.css';

export const Logo: FC = () => {
  return (
    <NavLink to="/" className="logo">
      <img src={logo} className="logo__image" alt="Логотип" />
    </NavLink>
  );
};
