import React, { FC, ImgHTMLAttributes } from 'react';
import classNames from 'classnames';
import './Image.css';

interface ImageProps extends ImgHTMLAttributes<HTMLImageElement> {
  skeleton?: boolean;
}

export const Image: FC<ImageProps> = ({ className, src = '', alt, onLoad, skeleton = false, ...restProps }) => {
  const [loaded, setLoaded] = React.useState(false);

  return (
    <div
      className={classNames(
        'image',
        {
          'image--loaded': loaded,
          'skeleton-gradient': skeleton || (src.length > 0 && !loaded),
        },
        className
      )}
    >
      {src.length > 0 && (
        <img
          {...restProps}
          onLoad={(e) => {
            setLoaded(true);
            onLoad && onLoad(e);
          }}
          className="image__element"
          src={src}
          alt={alt}
        />
      )}
    </div>
  );
};
