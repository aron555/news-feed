import React, { FC } from 'react';
import './Hero.css';
import classNames from 'classnames';
import { Title } from '../Title/Title';
import { Image } from '../Image/Image';
import { SkeletonText } from '@components/SkeletonText/SkeletonText';

interface IHeroSkeletonProps {
  hasImage?: boolean;
  title?: string;
  hasText?: boolean;
  className?: string;
}

export const HeroSkeleton: FC<IHeroSkeletonProps> = ({ hasImage = true, title, hasText = false, className }) => {
  return (
    <section
      className={classNames(
        'hero',
        {
          'hero--no-image': !hasImage,
        },
        className
      )}
    >
      <div className="hero__in">
        {hasImage && <Image className="hero__image" skeleton />}

        <div className="hero__container container">
          <div className="hero__content" style={{ width: title ? undefined : '100%' }}>
            <Title className="hero__title">{title || <SkeletonText />}</Title>
            {hasText && (
              <div className="hero__text">
                <SkeletonText rowCount={2} dark />
              </div>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};
