import React, { ElementType, FC, ReactNode } from 'react';
import './Title.css';
import classNames from 'classnames';

interface TitleProps {
  children?: ReactNode;
  Component?: ElementType;
  className?: string;
}

export const Title: FC<TitleProps> = ({ Component = 'h1', className, children }) => {
  return <Component className={classNames('title', className)}>{children}</Component>;
};
