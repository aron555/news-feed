import React, { FC } from 'react';
import classNames from 'classnames';
import './SkeletonText.css';
import { repeat } from '@app/utils';

interface SkeletonTextProps {
  rowCount?: number;
  dark?: boolean;
}

export const SkeletonText: FC<SkeletonTextProps> = ({ rowCount = 1, dark = false }: SkeletonTextProps) => {
  return (
    <span
      className={classNames('skeleton-text', {
        'skeleton-text--dark': dark,
      })}
    >
      {repeat((i) => {
        return <span key={i} className="skeleton-text__row skeleton-gradient"></span>;
      }, rowCount)}
    </span>
  );
};
