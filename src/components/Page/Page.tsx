import React, { FC, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Navigation } from '../Navigation/Navigation';
import './Page.css';
import { Logo } from '../Logo/Logo';
import { ColorSchemeSwitcher } from '@features/colorScheme/components/ColorSchemeSwitcher/ColorSchemeSwitcher';
import { EmailModal } from '@features/subscribeNotification/components/EmailModal/EmailModal';
import { Dispatch } from '@app/store';
import { fetchCategories } from '@features/categories/actions';
import { fetchSources } from '@features/sources/actions';

type TProps = {
  children?: React.ReactNode;
};

const LS_EMAIL_SHOWN_KEY = 'newsfeed:email_modal_shown';

export const Page: FC<TProps> = ({ children }) => {
  const [emailModalShown, setEmailModalShown] = useState(!localStorage.getItem(LS_EMAIL_SHOWN_KEY));

  const [emailTimer, setEmailTimer] = useState(false);

  const dispatch = useDispatch<Dispatch>();

  React.useEffect(() => {
    dispatch(fetchCategories());
    dispatch(fetchSources());

    const timer = setTimeout(() => {
      setEmailTimer(true);
    }, 60 * 1000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <>
      {emailTimer && (
        <EmailModal
          shown={emailModalShown}
          onClose={() => {
            localStorage.setItem(LS_EMAIL_SHOWN_KEY, 'true');
            setEmailModalShown(false);
          }}
        />
      )}

      <header className="header">
        <div className="container header__container">
          <Logo />
          <Navigation className="header__navigation" />
          <div className="header__controls">
            <ColorSchemeSwitcher />
          </div>
        </div>
      </header>

      <main>{children}</main>

      <footer className="footer">
        <div className="container">
          <div className="footer__top">
            <Logo />
            <Navigation className="footer__navigation" />
          </div>

          <div className="footer__bottom">
            <p>
              Сделано на Frontend курсе в{' '}
              <a rel="noreferrer" className="footer__link" href="https://karpov.courses/frontend" target="_blank">
                Karpov.Courses
              </a>
            </p>
            <p className="footer__text footer__text--gray">© {new Date().getFullYear()}</p>
          </div>
        </div>
      </footer>
    </>
  );
};
