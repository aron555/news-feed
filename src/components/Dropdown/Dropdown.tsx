import React, { FC, HTMLAttributes, RefObject, useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import throttle from 'lodash.throttle';
import './Dropdown.css';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

interface DropdownProps extends HTMLAttributes<HTMLElement> {
  targetRef: RefObject<HTMLElement>;
  shown: boolean;
  onShownChange: (shown: boolean) => void;
}

const calcCoords = (targetElement: HTMLElement) => {
  const rect = targetElement.getBoundingClientRect();

  return {
    top: window.screenY + rect.bottom + 12,
    right: window.innerWidth - rect.right - window.scrollX,
  };
};

export const Dropdown: FC<DropdownProps> = ({
  targetRef,
  shown,
  onShownChange,
  className,
  style,
  children,
  ...restProps
}: DropdownProps) => {
  const [coords, setCoords] = useState({ top: 0, right: 0 });

  const nodeRef = useRef(null);

  useEffect(() => {
    setCoords(() => calcCoords(targetRef.current as HTMLElement));
  }, []);

  useEffect(() => {
    onShownChange(shown);
  }, [shown, onShownChange]);

  useEffect(() => {
    const documentClickListener = (e: MouseEvent) => {
      const el = targetRef?.current as HTMLElement;
      const target = e?.target as Node;

      if (!el || !target) {
        return;
      }

      if (!el.contains(target)) {
        onShownChange(false);
      }
    };

    const windowResizeListener = throttle(() => {
      setCoords(() => calcCoords(targetRef.current as HTMLElement));
    }, 100);

    if (shown) {
      document.addEventListener('click', documentClickListener);
      window.addEventListener('resize', windowResizeListener);
    }

    return () => {
      document.removeEventListener('click', documentClickListener);
      document.removeEventListener('resize', windowResizeListener);
    };
  }, [onShownChange, shown]);

  return createPortal(
    <CSSTransition in={shown} nodeRef={nodeRef} timeout={200} classNames="dropdown-animation" unmountOnExit>
      <div ref={nodeRef} className={classNames('dropdown', className)} style={{ ...style, ...coords }} {...restProps}>
        {children}
      </div>
    </CSSTransition>,
    document.getElementById('overlay') as HTMLElement
  );
};
