import React from 'react';
import { createRoot } from 'react-dom/client';
import '@app/common.css';
import { AuthContextProvider } from '@features/auth/AuthContextProvider';
import { App } from '@app/components/App/App';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '@app/store';
import { initializeAPI } from '@app/api';

const firebaseApp = initializeAPI();

const container = document.getElementById('root') as HTMLBaseElement;
const root = createRoot(container);

root.render(
  <Provider store={store}>
    <AuthContextProvider firebaseApp={firebaseApp}>
      <Router>
        <App />
      </Router>
    </AuthContextProvider>
  </Provider>
);
