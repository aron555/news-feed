Веб приложение новостной ленты

STACK:
REACT, TYPESCRIPT, WEBPACK, BABEL, FIRESTORE,

# https://newsfeed.aron555.ru/

# https://newsfeed.aron555.ru/admin - админка

# Доступные скрипты

В каталоге проекта вы можете запустить:

# npm start

Запускает приложение в режиме разработки.

# npm run build

Создает приложение для прода в папке сборки.
Правильно связывает React в режиме прода и оптимизирует сборку для достижения наилучшей производительности.
Сборка минифицируется.

- настроен stylelint, чтобы проверять css

- настроен eslint, чтобы проверять ts

- настроен prettier, чтобы не фиксить несоответствия руками

- настроен прекоммит хук, чтобы при каждом коммите выполнялись проверки

- настроен editorconfig, чтобы настройки по отступам подтягивались у других

- настроен Firebase для хранения данных и аутентификации в админку

- Настроен gitlab-ci с проверками кода на качество и выкаткой кода на VDS при пуше в ветку master

Дизайн - https://www.figma.com/file/w75fNXskUgBGbWOlXW6WhM/News-Feed-1.0.?node-id=0%3A1
РеДизайн - https://www.figma.com/file/9cp2eCrj1MVJiMKS2Ok5Fx/Karpov-front-end?node-id=560%3A718

![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
